const menuList = [
  {
    title: 'Home',
    key: '/home',
  },
  {
    title: 'Order',
    key: '/order',
    btnList: [
      {
        title: 'Order Detail',
        key: 'detail',
      },
      {
        title: 'Order Finish',
        key: 'finish',
      },
    ],
  },
  {
    title: 'Map',
    key: '/map',
  },
  {
    title: 'Charts',
    key: '/charts',
    children: [
      {
        title: 'Bar Chart',
        key: '/charts/bar',
      },
      {
        title: 'Pie Chart',
        key: '/charts/pie',
      },
      {
        title: 'Line Chart',
        key: '/charts/line',
      },
    ],
  },
  {
    title: 'Permission',
    key: '/permission',
  },
];
export default menuList;
