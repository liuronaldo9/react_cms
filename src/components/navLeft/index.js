import React from 'react';
import { Menu } from 'antd';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { actionCreators } from '../header/store';
import MenuConfig from '../../resource/menuConfig.js';
import './index.less';

const SubMenu = Menu.SubMenu;
class NavLeft extends React.Component {
  componentWillMount() {
    const menuTreeNode = this.renderMenu(MenuConfig);
    this.setState({
      menuTreeNode,
    });
  }
  // render menu
  renderMenu = data => {
    return data.map(item => {
      if (item.children) {
        return (
          <SubMenu title={item.title} key={item.key}>
            {this.renderMenu(item.children)}
          </SubMenu>
        );
      }
      return (
        <Menu.Item title={item.title} key={item.key}>
          <NavLink to={item.key}>{item.title}</NavLink>
        </Menu.Item>
      );
    });
  };
  render() {
    return (
      <div>
        <NavLink to='/home' onClick={this.props.homeHandleClick}>
          <div className='logo'>
            <img
              src='https://www.realmadrid.com/StaticFiles/RealMadridResponsive/images/header_logo.svg'
              alt=''
            />
          </div>
        </NavLink>
        <Menu onClick={this.props.handleClick} theme='dark'>
          {this.state.menuTreeNode}
        </Menu>
      </div>
    );
  }
}

const mapDispatch = dispatch => ({
  handleClick({ item, key }) {
    dispatch(actionCreators.switchMenu(item.props.title));
  },
  homeHandleClick() {
    dispatch(actionCreators.switchMenu('Home'));
  },
});

export default connect(
  null,
  mapDispatch
)(NavLeft);
