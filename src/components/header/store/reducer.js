import * as constants from './actionTypes';

const defaultState = {
  menuName: '',
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.SWITCH_MENU:
      return {
        menuName: action.menuName,
      };
    default:
      return state;
  }
};
