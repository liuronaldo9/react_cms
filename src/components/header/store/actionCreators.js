import * as constants from './actionTypes';

const handleSwitchMenu = menuName => ({
  type: constants.SWITCH_MENU,
  menuName,
});

export const switchMenu = menuName => {
  return dispatch => {
    dispatch(handleSwitchMenu(menuName));
  };
};
