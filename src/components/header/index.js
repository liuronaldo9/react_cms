import React from 'react';
import { Row, Col } from 'antd';
import './index.less';
import Util from '../../utils/utils';
import { connect } from 'react-redux';
import { actionCreators as loginActionCreators } from '../../pages/login/store/';

class Header extends React.Component {
  state = {};
  componentWillMount() {
    setInterval(() => {
      let sysTime = Util.formateDate(new Date().getTime());
      this.setState({
        sysTime,
      });
    });
  }
  render() {
    const { menuName, menuType } = this.props;
    return (
      <div className='header'>
        <Row className='header-top'>
          {menuType ? (
            <Col span={6} className='logo'>
              <img
                src='https://www.realmadrid.com/StaticFiles/RealMadridResponsive/images/header_logo.svg'
                alt=''
              />
            </Col>
          ) : (
            ''
          )}
          <Col span={menuType ? 18 : 24}>
            <span>Welcome, {localStorage.getItem('user_name')}</span>
            <span className='logout-btn' onClick={this.props.handleLogout}>
              Logout
            </span>
          </Col>
        </Row>
        {menuType ? (
          ''
        ) : (
          <Row className='breadcrumb'>
            <Col span={4} className='breadcrumb-title'>
              {menuName || 'Home'}
            </Col>
            <Col span={20} className='right-side'>
              <span className='date'>{this.state.sysTime}</span>
            </Col>
          </Row>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    menuName: state.header.menuName,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleLogout() {
      dispatch(loginActionCreators.logout());
      window.location.reload();
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
