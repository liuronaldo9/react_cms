import React, { Component } from 'react';
import './index.less';

export default class Footer extends Component {
  render() {
    return (
      <div className='footer'>
        LinkedIn:
        <a href='https://www.linkedin.com/in/wenbo-joe-liu-14185b141/'>
          https://www.linkedin.com/in/wenbo-joe-liu-14185b141/
        </a>
      </div>
    );
  }
}
