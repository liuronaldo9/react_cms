import React from 'react';
import { Input, Select, Form, Button, Checkbox, DatePicker } from 'antd';
import Utils from '../../utils/utils';
const FormItem = Form.Item;

class FilterForm extends React.Component {
  handleFilterSubmit = () => {
    let fieldsValue = this.props.form.getFieldsValue();
    this.props.filterSubmit(fieldsValue);
  };

  reset = () => {
    this.props.form.resetFields();
  };

  initFormList = () => {
    const { getFieldDecorator } = this.props.form;
    const formList = this.props.formList;
    const formItemList = [];
    if (formList && formList.length > 0) {
      formList.forEach((item, i) => {
        let label = item.label;
        let field = item.field;
        let initialValue = item.initialValue || '';
        let placeholder = item.placeholder;
        if (item.type === 'BYTIME') {
          const begin_time = (
            <FormItem label='Order Time' key={i}>
              {getFieldDecorator('begin_time')(
                <DatePicker
                  showTime={true}
                  placeholder={placeholder}
                  format='YYYY-MM-DD HH:mm:ss'
                />
              )}
            </FormItem>
          );
          formItemList.push(begin_time);
          const end_time = (
            <FormItem label='~' colon={false} key={i + 1}>
              {getFieldDecorator('end_time')(
                <DatePicker
                  showTime={true}
                  placeholder={placeholder}
                  format='YYYY-MM-DD HH:mm:ss'
                />
              )}
            </FormItem>
          );
          formItemList.push(end_time);
        } else if (item.type === 'INPUT') {
          const INPUT = (
            <FormItem label={label} key={i + 2}>
              {getFieldDecorator([field], {
                initialValue: initialValue,
              })(<Input type='text' placeholder={placeholder} />)}
            </FormItem>
          );
          formItemList.push(INPUT);
        } else if (item.type === 'SELECT') {
          const SELECT = (
            <FormItem label={label} key={i + 3}>
              {getFieldDecorator([field], {
                initialValue: initialValue,
              })(
                <Select placeholder={placeholder}>
                  {Utils.getOptionList(item.list)}
                </Select>
              )}
            </FormItem>
          );
          formItemList.push(SELECT);
        } else if (item.type === 'CHECKBOX') {
          const CHECKBOX = (
            <FormItem label={label} key={i + 4}>
              {getFieldDecorator([field], {
                valuePropName: 'checked',
                initialValue: initialValue, //true | false
              })(<Checkbox>{label}</Checkbox>)}
            </FormItem>
          );
          formItemList.push(CHECKBOX);
        }
      });
    }
    return formItemList;
  };
  render() {
    return (
      <Form layout='inline'>
        {this.initFormList()}
        <FormItem>
          <Button
            type='primary'
            style={{ margin: '0 20px' }}
            onClick={this.handleFilterSubmit}
          >
            Search
          </Button>
          <Button onClick={this.reset}>Reset</Button>
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FilterForm);
