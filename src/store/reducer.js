import { combineReducers } from 'redux';
import { reducer as loginReaducer } from '../pages/login/store/';
import { reducer as headerReaducer } from '../components/header/store';

const reducer = combineReducers({
  login: loginReaducer,
  header: headerReaducer,
});

export default reducer;
