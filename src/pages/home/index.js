import React from 'react';
import './index.less';
export default class Home extends React.Component {
  render() {
    return (
      // eslint-disable-next-line jsx-a11y/accessible-emoji
      <div className='home-wrap'>Welcome, have a nice day! 🤙🤙🤙</div>
    );
  }
}
