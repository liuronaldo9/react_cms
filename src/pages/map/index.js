import React, { useState } from 'react';
import { Card } from 'antd';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';

function vendorMap() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [selectedMarker, setselectedMarker] = useState(null);
  let coordinates = [
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.812625,
      lng: 174.725129,
    },
    {
      name: 'open',
      lat: -36.892625,
      lng: 174.715129,
    },
    {
      name: 'open',
      lat: -36.882625,
      lng: 174.715129,
    },
    {
      name: 'open',
      lat: -36.812625,
      lng: 174.715129,
    },
    {
      name: 'open',
      lat: -36.892625,
      lng: 174.795129,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.785129,
    },
    {
      name: 'open',
      lat: -36.802625,
      lng: 174.715129,
    },
    {
      name: 'open',
      lat: -36.872625,
      lng: 174.791129,
    },
    {
      name: 'open',
      lat: -36.826625,
      lng: 174.754129,
    },
    {
      name: 'open',
      lat: -36.875725,
      lng: 174.799129,
    },
    {
      name: 'open',
      lat: -36.886825,
      lng: 174.799929,
    },
    {
      name: 'open',
      lat: -36.898365,
      lng: 174.792639,
    },
    {
      name: 'open',
      lat: -36.899995,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.888885,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.877775,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.812325,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.855555,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.866666,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.799999,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.788888,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.777777,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.766666,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.755555,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.744444,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.712345,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.767891,
    },
    {
      name: 'open',
      lat: -36.898765,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.843219,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.856473,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.892837,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.783645,
    },
    {
      name: 'open',
      lat: -36.883764,
      lng: 174.767543,
    },
    {
      name: 'open',
      lat: -36.869874,
      lng: 174.746543,
    },
    {
      name: 'open',
      lat: -36.811111,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.822222,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.999999,
      lng: 174.745129,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.786173,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.765756,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.789677,
    },
    {
      name: 'open',
      lat: -36.862625,
      lng: 174.767876,
    },
    {
      name: 'open',
      lat: -36.867534,
      lng: 174.712129,
    },
    {
      name: 'close',
      lat: -36.867565,
      lng: 174.742867,
    },
    {
      name: 'close',
      lat: -36.8256775,
      lng: 174.745633,
    },
    {
      name: 'close',
      lat: -36.865645,
      lng: 174.747856,
    },
    {
      name: 'close',
      lat: -36.813545,
      lng: 174.768468,
    },
    {
      name: 'close',
      lat: -36.828765,
      lng: 174.742139,
    },
    {
      name: 'close',
      lat: -36.864355,
      lng: 174.723519,
    },
    {
      name: 'close',
      lat: -36.821325,
      lng: 174.72329,
    },
    {
      name: 'close',
      lat: -36.863425,
      lng: 174.742129,
    },
    {
      name: 'close',
      lat: -36.867845,
      lng: 174.742129,
    },
  ];
  return (
    <GoogleMap
      defaultZoom={12}
      defaultCenter={{
        lat: -36.862625,
        lng: 174.745129,
      }}
    >
      {coordinates.map((marker, i) => {
        return (
          <Marker
            key={i}
            position={{
              lat: marker.lat,
              lng: marker.lng,
            }}
            onClick={() => {
              setselectedMarker(marker);
            }}
            icon={{
              url:
                marker.name === 'open'
                  ? 'https://s3-ap-southeast-2.amazonaws.com/familys.images/vendor-marker-open.png'
                  : 'https://s3-ap-southeast-2.amazonaws.com/familys.images/vendor-marker-close.png',
              scaledSize: new window.google.maps.Size(20, 20),
            }}
          />
        );
      })}
      {selectedMarker && (
        <InfoWindow
          position={{
            lat: selectedMarker.lat,
            lng: selectedMarker.lng,
          }}
          onCloseClick={() => {
            setselectedMarker(null);
          }}
        >
          <div>{selectedMarker.name}</div>
        </InfoWindow>
      )}
    </GoogleMap>
  );
}
const WrappedMap = withScriptjs(withGoogleMap(vendorMap));

export default class GMap extends React.Component {
  render() {
    return (
      <div>
        <Card style={{ marginTop: 10 }}>
          <div>Merchant Distribution Map</div>
          <div id='container' style={{ height: 500 }}>
            <WrappedMap
              googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyByb-EaPI_9__u_OqcpfoZebYtQan-FE94`}
              loadingElement={<div style={{ height: '100%' }} />}
              containerElement={
                <div
                  style={{ height: '100%', width: '90%', margin: '1px auto' }}
                />
              }
              mapElement={<div style={{ height: '100%' }} />}
            />
          </div>
        </Card>
      </div>
    );
  }
}
