import * as constants from './actionTypes';
// import axios from 'axios'

const handleLogin = account => ({
  type: constants.LOGIN_IN_SUCCESS,
  loginStatus: localStorage.getItem('login_status'),
  userName: localStorage.getItem('user_name'),
});

export const login = (account, password) => {
  return dispatch => {
    // axios.post('/').then(res => {
    //   const result =res.data.data
    // })
    localStorage.setItem('login_status', 'success');
    localStorage.setItem('user_name', account);
    dispatch(handleLogin());
  };
};

export const logout = () => {
  return dispatch => {
    localStorage.removeItem('login_status');
    localStorage.removeItem('user_name');
    dispatch(handleLogout());
  };
};

const handleLogout = () => ({
  type: constants.LOGOUT_SUCCESS,
  loginStatus: localStorage.getItem('login_status'),
  userName: localStorage.getItem('user_name'),
});
