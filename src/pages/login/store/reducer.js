import * as constants from './actionTypes';

const defaultState = {
  login: localStorage.getItem('login_status') || null,
  userName: localStorage.getItem('user_name') || null,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.LOGIN_IN_SUCCESS:
      return {
        login: action.loginStatus,
        userName: action.userName,
      };
    case constants.LOGOUT_SUCCESS:
      return {
        login: action.loginStatus,
        userName: action.userName,
      };
    default:
      return state;
  }
};
