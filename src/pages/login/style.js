import styled from 'styled-components';

export const LoginWrapper = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  z-index: 0;
  #myVideo {
    width: 100% 
    height: 100%
  }
`;
export const LoginBox = styled.div`
  position: absolute;
  width: 400px;
  height: 220px;
  padding-top: 20px;
  opacity: 0.8;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 8px;
  background: rgb(255, 255, 255);
  border-radius: 10px;
  .title {
    font-weight: 700;
    text-align: center;
    color: rgb(234, 111, 90);
    padding: 10px;
    border-bottom: 1px solid rgb(119, 119, 119);
  }
`;
export const Input = styled.input`
  display: block;
  width: 200px;
  height: 30px;
  line-height: 30px;
  color: rgb(119, 119, 119);
  padding: 0px 10px;
  margin: 10px auto;
  border-width: 1px;
  border-style: solid;
  border-color: rgb(119, 119, 119);
  border-image: initial;
`;
export const Button = styled.div`
  width: 220px;
  height: 30px;
  line-height: 30px;
  color: rgb(255, 255, 255);
  text-align: center;
  background: rgb(49, 148, 208);
  border-radius: 15px;
  margin: 10px auto;
`;
