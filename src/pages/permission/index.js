import React from 'react';
import {
  Card,
  Button,
  Form,
  Input,
  Select,
  Tree,
  Transfer,
  Modal,
  Table,
} from 'antd';
import menuConfig from '../../resource/menuConfig.js';
import Utils from '../../utils/utils';
const FormItem = Form.Item;
const Option = Select.Option;
const TreeNode = Tree.TreeNode;
export default class Order extends React.Component {
  state = {};

  componentWillMount() {
    this.requestList();
  }

  requestList = () => {
    this.setState({
      data: [
        {
          id: '1',
          role_name: 'Admin',
          status: 1,
          authorize_user_name: 'John Snow',
          create_time: '2019-05-01 21:51:11',
          menus: [
            '/home',
            '/order',
            '/map',
            '/charts',
            '/charts/bar',
            '/charts/pie',
            '/charts/line',
            '/permission',
          ],
        },
        {
          id: '2',
          role_name: 'Accountant',
          status: 1,
          authorize_user_name: 'Jaime Lannister',
          create_time: '2019-05-01 21:51:11',
          menus: [
            '/home',
            '/charts',
            '/charts/bar',
            '/charts/pie',
            '/charts/line',
            '/permission',
          ],
        },
        {
          id: '3',
          role_name: 'Super',
          status: 0,
          authorize_user_name: 'Daenerys Targaryen',
          create_time: '2019-05-01 21:51:11',
          menus: [
            '/home',
            '/order',
            '/map',
            '/charts',
            '/charts/bar',
            '/charts/pie',
            '/charts/line',
            '/permission',
          ],
        },
        {
          id: '4',
          role_name: 'Driver',
          status: 0,
          authorize_user_name: 'Jorah Mormont',
          create_time: '2019-05-01 21:51:11',
          menus: ['/home', '/order', '/map'],
        },
        {
          id: '5',
          role_name: 'Manager',
          status: 1,
          authorize_user_name: 'Sansa Stark',
          create_time: '2019-05-01 21:51:11',
          menus: ['/home', '/order'],
        },
      ],
    });
  };

  // Create Role
  handleRole = () => {
    this.setState({
      isRoleVisible: true,
    });
  };

  // Role Submit
  handleRoleSubmit = () => {
    this.setState({
      isRoleVisible: false,
    });
    this.requestList();
  };

  handlePermission = () => {
    if (!this.state.selectedItem) {
      Modal.info({
        title: 'Alert',
        content: 'Please select a user',
      });
      return;
    }
    this.setState({
      isPermVisible: true,
      detailInfo: this.state.selectedItem,
    });
    let menuList = this.state.selectedItem.menus;
    this.setState({
      menuInfo: menuList,
    });
  };

  handlePermEditSubmit = () => {
    let data = this.roleForm.props.form.getFieldsValue();
    data.role_id = this.state.selectedItem.id;
    data.menus = this.state.menuInfo;
    this.setState({
      isPermVisible: false,
    });
    this.requestList();
  };

  // User Auth
  handleUserAuth = () => {
    if (!this.state.selectedItem) {
      Modal.info({
        title: 'Alert',
        content: 'Please select a user',
      });
      return;
    }
    this.getRoleUserList();
    this.setState({
      isUserVisible: true,
      isAuthClosed: false,
      detailInfo: this.state.selectedItem,
    });
  };

  getRoleUserList = () => {
    const result = [
      {
        status: 1,
        user_id: 1,
        user_name: 'Arya Stark',
      },
      {
        status: 1,
        user_id: 2,
        user_name: 'Robb Stark',
      },
      {
        status: 0,
        user_id: 3,
        user_name: 'Theon Greyjoy',
      },
      {
        status: 0,
        user_id: 4,
        user_name: 'Joffrey Baratheon',
      },
      {
        status: 1,
        user_id: 5,
        user_name: 'Tyrion Lannister',
      },
      {
        status: 1,
        user_id: 6,
        user_name: 'Khal Drogo',
      },
      {
        status: 0,
        user_id: 7,
        user_name: 'Davos Seaworth',
      },
      {
        status: 0,
        user_id: 8,
        user_name: 'Samwell Tarly',
      },
      {
        status: 0,
        user_id: 9,
        user_name: 'Stannis Baratheon',
      },
      {
        status: 0,
        user_id: 10,
        user_name: 'Melisandre',
      },
      {
        status: 0,
        user_id: 11,
        user_name: 'Lyanna Mormont',
      },
      {
        status: 1,
        user_id: 12,
        user_name: 'Brienne of Tarth',
      },
    ];
    this.getAuthUserList(result);
  };

  // Select User
  getAuthUserList = dataSource => {
    const mockData = [];
    const targetKeys = [];
    if (dataSource && dataSource.length > 0) {
      for (let i = 0; i < dataSource.length; i++) {
        const data = {
          key: dataSource[i].user_id,
          title: dataSource[i].user_name,
          status: dataSource[i].status,
        };
        if (data.status === 1) {
          targetKeys.push(data.key);
        }
        mockData.push(data);
      }
    }
    this.setState({ mockData, targetKeys });
  };

  patchUserInfo = targetKeys => {
    this.setState({
      targetKeys: targetKeys,
    });
  };

  onRowClick = (record, id) => {
    let selectKey = [id];
    this.setState({
      selectedRowKeys: selectKey,
      selectedItem: record,
    });
  };

  // Auth
  handleUserSubmit = () => {
    this.setState({
      isUserVisible: false,
    });
    this.requestList();
  };
  render() {
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
      },
      {
        title: 'Name',
        dataIndex: 'role_name',
      },
      {
        title: 'Create Time',
        dataIndex: 'create_time',
        render: Utils.formatTime,
      },
      {
        title: 'Status',
        dataIndex: 'status',
        render(status) {
          if (status === 1) {
            return 'Active';
          } else {
            return 'Block';
          }
        },
      },
    ];
    const selectedRowKeys = this.state.selectedRowKeys;
    const rowSelection = {
      type: 'radio',
      selectedRowKeys,
    };
    return (
      <div>
        <Card>
          <Button type='primary' onClick={this.handleRole}>
            Create Role
          </Button>
          <Button type='primary' onClick={this.handlePermission}>
            Set Role
          </Button>
          <Button type='primary' onClick={this.handleUserAuth}>
            Set User
          </Button>
        </Card>
        <div className='content-wrap'>
          <Table
            bordered
            columns={columns}
            dataSource={this.state.data}
            rowSelection={rowSelection}
            rowKey='id'
            onRow={(record, rowKey) => {
              return {
                onClick: () => {
                  this.onRowClick(record, record.id);
                },
              };
            }}
          />
        </div>
        <Modal
          title='Create Role'
          visible={this.state.isRoleVisible}
          onOk={this.handleRoleSubmit}
          onCancel={() => {
            this.roleForm.props.form.resetFields();
            this.setState({
              isRoleVisible: false,
            });
          }}
        >
          <RoleForm wrappedComponentRef={inst => (this.roleForm = inst)} />
        </Modal>
        <Modal
          title='Set Role'
          visible={this.state.isPermVisible}
          width={600}
          onOk={this.handlePermEditSubmit}
          onCancel={() => {
            this.setState({
              isPermVisible: false,
            });
          }}
        >
          <PermEditForm
            wrappedComponentRef={inst => (this.roleForm = inst)}
            detailInfo={this.state.detailInfo}
            menuInfo={this.state.menuInfo || []}
            patchMenuInfo={checkedKeys => {
              this.setState({
                menuInfo: checkedKeys,
              });
            }}
          />
        </Modal>
        <Modal
          title='Set User'
          visible={this.state.isUserVisible}
          width={800}
          onOk={this.handleUserSubmit}
          onCancel={() => {
            this.setState({
              isUserVisible: false,
            });
          }}
        >
          <RoleAuthForm
            wrappedComponentRef={inst => (this.userAuthForm = inst)}
            isClosed={this.state.isAuthClosed}
            detailInfo={this.state.detailInfo}
            targetKeys={this.state.targetKeys}
            mockData={this.state.mockData}
            patchUserInfo={this.patchUserInfo}
          />
        </Modal>
      </div>
    );
  }
}

// Create Role
class RoleForm extends React.Component {
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 16 },
    };
    return (
      <Form layout='horizontal'>
        <FormItem label='Name' {...formItemLayout}>
          {getFieldDecorator('role_name', {
            initialValue: '',
          })(<Input type='text' placeholder='Please add a name' />)}
        </FormItem>
        <FormItem label='State' {...formItemLayout}>
          {getFieldDecorator('state', {
            initialValue: 1,
          })(
            <Select>
              <Option value={1}>Active</Option>
              <Option value={0}>Block</Option>
            </Select>
          )}
        </FormItem>
      </Form>
    );
  }
}
RoleForm = Form.create({})(RoleForm);

// set Role
class PermEditForm extends React.Component {
  state = {};
  // Set selected item，get from props
  onCheck = checkedKeys => {
    this.props.patchMenuInfo(checkedKeys);
  };
  renderTreeNodes = (data, key = '') => {
    return data.map(item => {
      let parentKey = key + item.key;
      if (item.children) {
        return (
          <TreeNode
            title={item.title}
            key={parentKey}
            dataRef={item}
            className='op-role-tree'
          >
            {this.renderTreeNodes(item.children, parentKey)}
          </TreeNode>
        );
      } else if (item.btnList) {
        return (
          <TreeNode
            title={item.title}
            key={parentKey}
            dataRef={item}
            className='op-role-tree'
          >
            {this.renderBtnTreedNode(item, parentKey)}
          </TreeNode>
        );
      }
      return <TreeNode {...item} />;
    });
  };

  handleChange = value => {
    // console.log(`selected ${value}`);
  };
  renderBtnTreedNode = (menu, parentKey = '') => {
    const btnTreeNode = [];
    menu.btnList.forEach(item => {
      btnTreeNode.push(
        <TreeNode
          title={item.title}
          key={parentKey + '-btn-' + item.key}
          className='op-role-tree'
        />
      );
    });
    return btnTreeNode;
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 18 },
    };
    const detail_info = this.props.detailInfo;
    const menuInfo = this.props.menuInfo;
    return (
      <Form layout='horizontal'>
        <FormItem label='Name:' {...formItemLayout}>
          <Input disabled maxLength={8} placeholder={detail_info.role_name} />
        </FormItem>
        <FormItem label='Status:' {...formItemLayout}>
          {getFieldDecorator('status', {
            initialValue: detail_info.status === 1 ? '1' : '0',
          })(
            <Select style={{ width: 80 }} onChange={this.handleChange()}>
              <Option value='1'>Avtive</Option>
              <Option value='0'>Block</Option>
            </Select>
          )}
        </FormItem>
        <Tree
          checkable
          defaultExpandAll
          onCheck={checkedKeys => this.onCheck(checkedKeys)}
          checkedKeys={menuInfo || []}
        >
          <TreeNode title='Authorisation' key='platform_all'>
            {this.renderTreeNodes(menuConfig)}
          </TreeNode>
        </Tree>
      </Form>
    );
  }
}

PermEditForm = Form.create({})(PermEditForm);

// Set User
class RoleAuthForm extends React.Component {
  filterOption = (inputValue, option) => {
    return option.title.indexOf(inputValue) > -1;
  };
  handleChange = targetKeys => {
    this.props.patchUserInfo(targetKeys);
  };

  render() {
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 18 },
    };
    const detail_info = this.props.detailInfo;
    return (
      <Form layout='horizontal'>
        <FormItem label='Name:' {...formItemLayout}>
          <Input disabled maxLength={8} placeholder={detail_info.role_name} />
        </FormItem>
        <FormItem label='Select User:' {...formItemLayout}>
          <Transfer
            listStyle={{ width: 200, height: 400 }}
            dataSource={this.props.mockData}
            showSearch
            titles={['Users', 'Select']}
            searchPlaceholder='add a name'
            filterOption={this.filterOption}
            targetKeys={this.props.targetKeys}
            onChange={this.handleChange}
            render={item => item.title}
          />
        </FormItem>
      </Form>
    );
  }
}
RoleAuthForm = Form.create({})(RoleAuthForm);
