import React, { useState } from 'react';
import { Card } from 'antd';
import './detail.less';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';

function GMap() {
  const [selectedMarker, setselectedMarker] = useState(null);
  let coordinates = [
    {
      name: 'vendor',
      lat: -37.632658,
      lng: 176.178987,
    },
    {
      name: 'user',
      lat: -37.657228,
      lng: 176.204916,
    },
  ];
  return (
    <GoogleMap
      defaultZoom={12}
      defaultCenter={{
        lat: -37.630586,
        lng: 176.172238,
      }}
    >
      {coordinates.map((marker, i) => {
        return (
          <Marker
            key={i}
            position={{
              lat: marker.lat,
              lng: marker.lng,
            }}
            onClick={() => {
              setselectedMarker(marker);
            }}
            icon={{
              url:
                marker.name === 'vendor'
                  ? 'https://s3-ap-southeast-2.amazonaws.com/familys.images/vendor-marker-open.png'
                  : 'https://s3-ap-southeast-2.amazonaws.com/familys.images/home.png',
              scaledSize: new window.google.maps.Size(30, 30),
            }}
          />
        );
      })}
      {selectedMarker && (
        <InfoWindow
          position={{
            lat: selectedMarker.lat,
            lng: selectedMarker.lng,
          }}
          onCloseClick={() => {
            setselectedMarker(null);
          }}
        >
          <div>{selectedMarker.name}</div>
        </InfoWindow>
      )}
    </GoogleMap>
  );
}
const WrappedMap = withScriptjs(withGoogleMap(GMap));

export default class Order extends React.Component {
  state = {};

  componentDidMount() {
    let orderId = this.props.match.params.orderId;
    if (orderId) {
      this.getDetailInfo(orderId);
    }
  }

  getDetailInfo = orderId => {
    console.log(orderId);
  };

  render() {
    return (
      <div>
        <Card>
          <div id='orderDetailMap' className='order-map'>
            <WrappedMap
              googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyByb-EaPI_9__u_OqcpfoZebYtQan-FE94`}
              loadingElement={<div style={{ height: '100%' }} />}
              containerElement={
                <div
                  style={{ height: '100%', width: '90%', margin: '1px auto' }}
                />
              }
              mapElement={<div style={{ height: '100%' }} />}
            />
          </div>
          <div className='detail-items'>
            <div className='item-title'>Order Information</div>
            <ul className='detail-form'>
              <li>
                <div className='detail-form-left'>Order ID</div>
                <div className='detail-form-content'>009</div>
              </li>
              <li>
                <div className='detail-form-left'>Client Name</div>
                <div className='detail-form-content'>Joe</div>
              </li>
              <li>
                <div className='detail-form-left'>Phone</div>
                <div className='detail-form-content'>021 000 999</div>
              </li>
            </ul>
          </div>
          <div className='detail-items'>
            <div className='item-title'>Driver Information</div>
            <ul className='detail-form'>
              <li>
                <div className='detail-form-left'>Start Time</div>
                <div className='detail-form-content'>2019-03-21 20:11:11</div>
              </li>
              <li>
                <div className='detail-form-left'>End Time</div>
                <div className='detail-form-content'>2019-03-21 20:41:11</div>
              </li>
              <li>
                <div className='detail-form-left'>Distance</div>
                <div className='detail-form-content'>5 Km</div>
              </li>
            </ul>
          </div>
        </Card>
      </div>
    );
  }
}
