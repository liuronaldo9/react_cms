import React from 'react';
import { Card, Button, Table, Form, Modal } from 'antd';
import BaseForm from '../../components/BaseForm';
const FormItem = Form.Item;

export default class Order extends React.Component {
  state = {
    orderInfo: {},
    orderConfirmVisble: false,
  };
  params = {
    page: 1,
  };
  formList = [
    {
      type: 'SELECT',
      label: 'Area',
      field: 'Suburb',
      placeholder: 'ALL',
      initialValue: '1',
      list: [
        { id: '0', name: 'ALL' },
        { id: '1', name: 'Auckland City' },
        { id: '2', name: 'North Shore' },
        { id: '3', name: 'Henderson' },
      ],
    },
    {
      type: 'BYTIME',
    },
    {
      type: 'SELECT',
      label: 'Order Status',
      field: 'order_status',
      placeholder: 'ALL',
      initialValue: '1',
      list: [
        { id: '0', name: 'ALL' },
        { id: '1', name: 'Piking' },
        { id: '2', name: 'Finish' },
      ],
    },
  ];
  componentDidMount() {
    this.requestList();
  }

  handleFilter = params => {
    this.params = params;
    this.requestList();
  };
  requestList = () => {
    this.setState({
      data: [
        {
          id: '1',
          prodect: 'aunt',
          user_name: 'John Snow',
          Phone: '0210000000',
          distance: 2000,
          status: 'picking',
          start_time: '2019-05-01 21:51:11',
          fee: 30.0,
        },
        {
          id: '2',
          prodect: 'boar',
          user_name: 'Robert Baratheon',
          Phone: '02104534000',
          distance: 1000,
          status: 'picking',
          start_time: '2019-04-01 01:50:08',
          fee: 90.0,
        },
        {
          id: '3',
          prodect: 'sister',
          user_name: 'Jaime Lannister',
          Phone: '0210675600',
          distance: 1900,
          status: 'finish',
          start_time: '2019-03-01 13:54:11',
          fee: 28.0,
        },
        {
          id: '4',
          prodect: 'elephant',
          user_name: 'Cersei Lannister',
          Phone: '021767800',
          distance: 5000,
          status: 'picking',
          start_time: '2019-05-11 12:23:52',
          fee: 67.0,
        },
        {
          id: '5',
          prodect: 'peanut',
          user_name: 'Viserys Targaryen',
          Phone: '021089676',
          distance: 9000,
          status: 'picking',
          start_time: '2019-03-21 20:11:11',
          fee: 51.0,
        },
        {
          id: '6',
          prodect: 'dragon',
          user_name: 'Daenerys Targaryen',
          Phone: '021008900',
          distance: 1090,
          status: 'picking',
          start_time: '2019-04-21 15:41:27',
          fee: 82.0,
        },
        {
          id: '7',
          prodect: 'Khaleesi',
          user_name: 'Jorah Mormont',
          Phone: '021086780',
          distance: 5000,
          status: 'picking',
          start_time: '2019-04-8 12:20:10',
          fee: 17.0,
        },
        {
          id: '8',
          prodect: 'wheelchair',
          user_name: 'Bran Stark',
          Phone: '02101231200',
          distance: 3000,
          status: 'picking',
          start_time: '2019-05-30 14:56:37',
          fee: 32.0,
        },
        {
          id: '9',
          prodect: 'freedom',
          user_name: 'Sansa Stark',
          Phone: '0210654600',
          distance: 2000,
          status: 'picking',
          start_time: '2019-05-19 09:09:11',
          fee: 60.0,
        },
      ],
    });
  };
  handleConfirm = () => {
    let item = this.state.selectedItem;
    console.log(item);
    if (!item) {
      Modal.info({
        title: 'Infomation',
        content: 'Please select one order',
      });
      return;
    }
    this.setState({
      orderInfo: item,
      orderConfirmVisble: true,
    });
  };

  handleFinishOrder = () => {
    this.setState({
      orderConfirmVisble: false,
    });
  };
  onRowClick = (record, id) => {
    let selectKey = [id];
    this.setState({
      selectedRowKeys: selectKey,
      selectedItem: record,
    });
  };

  openOrderDetail = () => {
    let item = this.state.selectedItem;
    if (!item) {
      Modal.info({
        title: 'Infomation',
        content: 'Please select one order',
      });
      return;
    }
    window.open(`/common/order/detail/${item.id}`, '_blank');
  };

  render() {
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
      },
      {
        title: 'Prdouct',
        dataIndex: 'prodect',
      },
      {
        title: 'User',
        dataIndex: 'user_name',
      },
      {
        title: 'Phone',
        dataIndex: 'Phone',
      },
      {
        title: 'Distance',
        dataIndex: 'distance',
        render(distance) {
          return distance / 1000 + 'Km';
        },
      },
      {
        title: 'Status',
        dataIndex: 'status',
      },
      {
        title: 'Fee',
        dataIndex: 'fee',
      },
      {
        title: 'Order Time',
        dataIndex: 'start_time',
      },
    ];
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 },
    };
    const selectedRowKeys = this.state.selectedRowKeys;
    const rowSelection = {
      type: 'radio',
      selectedRowKeys,
    };
    return (
      <div>
        <Card>
          <BaseForm formList={this.formList} filterSubmit={this.handleFilter} />
        </Card>
        <Card style={{ marginTop: 10 }}>
          <Button type='primary' onClick={this.openOrderDetail}>
            Order Detail
          </Button>
          <Button
            type='primary'
            style={{ marginLeft: 10 }}
            onClick={this.handleConfirm}
          >
            Close Order
          </Button>
        </Card>
        <div className='content-wrap'>
          <Table
            bordered
            columns={columns}
            dataSource={this.state.data}
            rowSelection={rowSelection}
            rowKey='id'
            onRow={(record, rowKey) => {
              return {
                onClick: () => {
                  this.onRowClick(record, record.id);
                },
              };
            }}
          />
        </div>
        <Modal
          title='Close Order'
          visible={this.state.orderConfirmVisble}
          onCancel={() => {
            this.setState({
              orderConfirmVisble: false,
            });
          }}
          onOk={this.handleFinishOrder}
          width={600}
        >
          <Form layout='horizontal'>
            <FormItem label='Order ID' {...formItemLayout}>
              {this.state.orderInfo.id}
            </FormItem>
            <FormItem label='Client Name' {...formItemLayout}>
              {this.state.orderInfo.user_name}
            </FormItem>
            <FormItem label='Phone' {...formItemLayout}>
              {this.state.orderInfo.Phone}
            </FormItem>
            <FormItem label='Fee' {...formItemLayout}>
              {this.state.orderInfo.fee}
            </FormItem>
          </Form>
        </Modal>
      </div>
    );
  }
}
