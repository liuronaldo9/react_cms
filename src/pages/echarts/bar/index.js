import React from 'react';
import { Card } from 'antd';
import ReactEcharts from 'echarts-for-react';
import echartTheme from '../echartTheme';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markPoint';
export default class Bar extends React.Component {
  state = {};

  componentWillMount() {
    echarts.registerTheme('Imooc', echartTheme);
  }

  getOption() {
    let option = {
      title: {
        text: 'Income',
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        data: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name: 'Icome',
          type: 'bar',
          data: [1000, 2000, 1500, 3000, 2000, 1200, 800],
        },
      ],
    };
    return option;
  }

  getOption2() {
    let option = {
      title: {
        text: 'Order',
      },
      tooltip: {
        trigger: 'axis',
      },
      legend: {
        data: ['All', 'Canceled', 'Unpay'],
      },
      xAxis: {
        data: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name: 'All',
          type: 'bar',
          data: [200, 1000, 500, 700, 400, 700, 950],
        },
        {
          name: 'Canceled',
          type: 'bar',
          data: [50, 20, 76, 90, 30, 120, 90],
        },
        {
          name: 'Unpay',
          type: 'bar',
          data: [10, 10, 56, 20, 30, 23, 67],
        },
      ],
    };
    return option;
  }

  render() {
    return (
      <div>
        <Card title='Income'>
          <ReactEcharts
            option={this.getOption()}
            theme='Imooc'
            notMerge={true}
            lazyUpdate={true}
            style={{ height: 500 }}
          />
        </Card>
        <Card title='Order' style={{ marginTop: 10 }}>
          <ReactEcharts
            option={this.getOption2()}
            theme='Imooc'
            notMerge={true}
            lazyUpdate={true}
            style={{ height: 500 }}
          />
        </Card>
      </div>
    );
  }
}
