import React from 'react';
import { Card } from 'antd';
import ReactEcharts from 'echarts-for-react';
import echartTheme from '../echartTheme';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markPoint';
export default class Line extends React.Component {
  state = {};

  componentWillMount() {
    echarts.registerTheme('Imooc', echartTheme);
  }

  getOption() {
    let option = {
      title: {
        text: 'Income',
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        data: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name: 'Income',
          type: 'line',
          data: [700, 1000, 1500, 300, 900, 1200, 1000],
        },
      ],
    };
    return option;
  }

  getOption2() {
    let option = {
      title: {
        text: 'Order',
      },
      tooltip: {
        trigger: 'axis',
      },
      legend: {
        data: ['All', 'Canceled'],
      },
      xAxis: {
        data: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name: 'All',
          type: 'line',
          stack: 'All',
          data: [200, 1000, 500, 700, 400, 700, 950],
        },
        {
          name: 'Canceled',
          type: 'line',
          stack: 'Canceled',
          data: [50, 20, 76, 90, 30, 120, 90],
        },
      ],
    };
    return option;
  }

  getOption3() {
    let option = {
      title: {
        text: 'Order',
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name: 'Order',
          type: 'line',
          data: [200, 1000, 500, 700, 400, 700, 950],
          areaStyle: {},
        },
      ],
    };
    return option;
  }

  render() {
    return (
      <div>
        <Card title='Income'>
          <ReactEcharts
            option={this.getOption()}
            theme='Imooc'
            notMerge={true}
            lazyUpdate={true}
            style={{
              height: 500,
            }}
          />
        </Card>
        <Card title='Order' style={{ marginTop: 10 }}>
          <ReactEcharts
            option={this.getOption2()}
            theme='Imooc'
            notMerge={true}
            lazyUpdate={true}
            style={{
              height: 500,
            }}
          />
        </Card>
        <Card title='Order' style={{ marginTop: 10 }}>
          <ReactEcharts
            option={this.getOption3()}
            theme='Imooc'
            notMerge={true}
            lazyUpdate={true}
            style={{
              height: 500,
            }}
          />
        </Card>
      </div>
    );
  }
}
