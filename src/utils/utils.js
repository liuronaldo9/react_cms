import React from 'react';
import { Select } from 'antd';
const Option = Select.Option;
export default {
  formateDate(time) {
    if (!time) return '';
    let date = new Date(time);
    return (
      date.getFullYear() +
      '-' +
      (date.getMonth() + 1) +
      '-' +
      date.getDate() +
      ' ' +
      date.getHours() +
      ':' +
      date.getMinutes() +
      ':' +
      date.getSeconds()
    );
  },

  getOptionList(data) {
    if (!data) {
      return [];
    }
    let options = [];
    // eslint-disable-next-line array-callback-return
    data.map(item => {
      // console.log(item)
      options.push(
        <Option value={item.id} key={item.id}>
          {item.name}
        </Option>
      );
    });
    return options;
  },

  updateSelectedItem(selectedRowKeys, selectedRows, selectedIds) {
    if (selectedIds) {
      this.setState({
        selectedRowKeys,
        selectedIds: selectedIds,
        selectedItem: selectedRows,
      });
    } else {
      this.setState({
        selectedRowKeys,
        selectedItem: selectedRows,
      });
    }
  },
};
