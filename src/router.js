import React from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import App from './App';
import Login from './pages/login';
import Admin from './admin';
import Home from './pages/home';
import Order from './pages/order';
import Common from './common';
import OrderDetail from './pages/order/detail';
import GMap from './pages/map';
import Bar from './pages/echarts/bar';
import Pie from './pages/echarts/pie';
import Line from './pages/echarts/line';
import Permission from './pages/permission';

export default class ERouter extends React.Component {
  render() {
    return (
      <HashRouter>
        <App>
          <Switch>
            <Route path='/login' component={Login} />
            <Route
              path='/common'
              render={() => (
                <Common>
                  <Route
                    path='/common/order/detail/:orderId'
                    component={OrderDetail}
                  />
                </Common>
              )}
            />
            <Route
              path='/'
              render={() =>
                localStorage.getItem('login_status') !== 'success' ? (
                  <Redirect to='/login' />
                ) : (
                  <Admin>
                    <Switch>
                      <Route path='/home' component={Home} />
                      <Route path='/order' component={Order} />
                      <Route path='/Map' component={GMap} />
                      <Route path='/charts/bar' component={Bar} />
                      <Route path='/charts/pie' component={Pie} />
                      <Route path='/charts/line' component={Line} />
                      <Route path='/permission' component={Permission} />
                      {/* <Route component={NoMatch} /> */}
                    </Switch>
                  </Admin>
                )
              }
            />
          </Switch>
        </App>
      </HashRouter>
    );
  }
}
